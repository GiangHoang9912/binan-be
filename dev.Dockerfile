FROM node:17-alpine as builder
# Set the working directory.
RUN  mkdir /usr/src/app -p
WORKDIR /usr/src/app

COPY package.json .
COPY yarn.lock .
RUN yarn install
COPY . .

RUN yarn build

CMD [ "yarn", "start:dev" ]
# Copy the file from your host to your current location.
