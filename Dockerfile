FROM node:17-alpine as builder
# Set the working directory.
RUN  mkdir /usr/src/app -p
WORKDIR /usr/src/app

COPY package.json .
COPY yarn.lock .
RUN yarn install

COPY . .
RUN npx prisma generate

#run command before
#RUN yarn global add pm2

RUN yarn build

FROM node:17-alpine as runner
WORKDIR /usr/src/app
COPY prisma .
COPY --from=builder /usr/src/app/package.json .
COPY --from=builder /usr/src/app/yarn.lock .
# Run the command inside your image filesystem.
RUN yarn install --production=true
COPY --from=builder /usr/src/app/dist dist
RUN npx prisma generate

ENV HOST=0.0.0.0
ENV APP_PORT=8081

EXPOSE 8081

CMD [ "yarn", "start" ]
# Copy the file from your host to your current location.
