import { helper } from "../libs/helper";
import Queue, { Job } from "bull";
import { getConfig } from "../libs/getConfig";
import { Models } from "../libs/db/models";
import { where } from "ramda";

export const ProcessJobRunAutoQueue = new Queue(
  "ProcessJobRunAuto",
  getConfig("REDIS_URI"),
  {
    limiter: {
      max: 5,
      duration: 1000,
      bounceBack: false,
    },
  }
);

ProcessJobRunAutoQueue.process(async function (job: Job) {
  try {
    return true;
  } catch (e) {
    console.log(e);
  }
});

export const initWorkerJobMaster = async () => {
  // await ProcessCrawlInUrlJob.initWorker();
  ProcessJobRunAutoQueue.add(
    { type: "AUTO_RUN" },
    {
      repeat: { every: 10000 },
      removeOnComplete: 20,
      removeOnFail: 20,
    }
  );
};
