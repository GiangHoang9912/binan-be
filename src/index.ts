
import http from "http";
import { httpServer } from "./http";
// import { init } from "./init";
import { initAdapter } from "./monitorQueue";
import { getConfig } from "./libs/getConfig";
import { createWsServer } from "./ws";
// import { connectRedis } from "./libs/cache_helper";


async function main() {
    // connectRedis();
    const server = http.createServer(httpServer.callback());
    createWsServer(server);
    server.listen(getConfig("APP_PORT"));
    await initAdapter();
    // await init();

    console.log(`App "${(getConfig("app.name") + " dev")}" run on ${getConfig("APP_PORT")}`);
}

main();
