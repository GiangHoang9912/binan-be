import Koa from "koa";
import Router from "@koa/router";
import { ApiSuccessHandler } from "../../libs/koa_helper";
import { RestFindDefault } from "../../types";
import { authUser } from "../auth/auth.middleware";
import {UserService as Service} from "./user.service";

export const Controller = new Koa();
const router = new Router();

router.get(
    "/",
    ...[
        authUser(),
        async function listCategories(ctx: Koa.Context, next: Koa.Next) {
            const params = { ...RestFindDefault, ...ctx.request.query };
            const { data, message } = await Service.getUserInfor(params);
            ApiSuccessHandler.sendJson(ctx, "ok", params);
            await next();
        },
    ]
);

Controller.use(router.routes()).use(router.allowedMethods());

export default Controller;
