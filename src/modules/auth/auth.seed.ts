import { Prisma } from "@prisma/client";
import { Models } from "../../libs/db/models";
import { helper } from "../../libs/helper";

async function seeding() {
    const record: Prisma.UserCreateInput = {
        id: "admin",
        email: "gianghoang.9955@gmail.com",
        phone: "0819169868",
        password: helper.getSha1("12345678"),
        inviteCode: "admin1234",
        token: "admin1234"
    };
    const recordInDb = await Models.user.findFirst({ where: { id: record.id } });
    if (!recordInDb) {
        await Models.user.create({ data: record });
    }
    return {
        record,
    };
}

export default seeding;
