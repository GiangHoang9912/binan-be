import Koa from "koa";
import { AuthServices } from "./auth.services";
import { MyError } from "../../libs/errors";
import { Models } from "../../libs/db/models";
import { AuthType } from "./auth.types";
import { User } from "@prisma/client";

export function authUser(params = {}): Koa.Middleware {
    return async function authUser(ctx: Koa.Context, next: Koa.Next) {
        let accessToken = ctx?.session?.accessToken;
        const authHeader = String(ctx.request.headers["authorization"] || "");
        if (authHeader.startsWith("Bearer ")) {
            accessToken = authHeader.substring(7, authHeader.length);
        }
        let decode: AuthType.AccessTokenPayload;
        try {
            decode = await AuthServices.verifyAccessToken(accessToken);
        } catch (err) {
            console.log(err);
            throw MyError.NotAuthen("User session expired, you should login");
        }

        const { user } = decode;
        if (!user || !user.id) {
            throw MyError.NotAuthen("User session expired, you should login");
        }
        const userData: User = await Models.user.findFirst({ where: { id: user.id } });
        if (!userData) {
            throw MyError.NotAuthen("Invalid user");
        }

        ctx.user = userData;
        await next();
    };
}

