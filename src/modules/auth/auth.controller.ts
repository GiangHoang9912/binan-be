import * as fs from 'fs';
import { helper } from './../../libs/helper';
import Koa from "koa";
import Router from "@koa/router";
import { ApiSuccessHandler } from "../../libs/koa_helper";
import { apiHelper } from "../../libs/api_helper";
import { AuthServices as Service } from "./auth.services";
import { AuthLoginPhoneDto, AuthLoginEmailDto, AuthRegisterDto, RefreshTokenDto } from "./auth.dto";
import { authUser } from "./auth.middleware";
import { getConfig } from '../../libs/getConfig';

export const Controller = new Koa();
const router = new Router();

router.post(
    "/register",
    ...[
        async function create(ctx: Koa.Context, next: Koa.Next) {
            const record = await apiHelper.validator.validate(ctx.request.body, AuthRegisterDto);
            console.log(ctx.request.body)
            const { data, message } = await Service.register(record);
            if (data.user.email) {
                // get email template
                const template = fs.readFileSync("src/modules/email-template/verify-email.html")
                const html = template.toString()
                // send mail
                helper.sendmail({
                    email: data.user.email,
                    content: html,
                    subject: "Verify email",
                    text: "",
                    context: {
                        host: getConfig("LOCAL_HOST"),
                        token: data.user.token,
                        text: "Verify",
                        userId: data.user.id
                    }
                })
            }
            ApiSuccessHandler.sendJson(ctx, message, data);
            await next();
        },
    ]
);
router.post(
    "/login-email",
    ...[
        async function login(ctx: Koa.Context, next: Koa.Next) {
            const record = await apiHelper.validator.validate(ctx.request.body, AuthLoginEmailDto);
            const { data, message } = await Service.loginWithEmail(record);
            ApiSuccessHandler.sendJson(ctx, message, data);
            await next();
        },
    ]
);

router.post(
    "/login-phone",
    ...[
        async function login(ctx: Koa.Context, next: Koa.Next) {
            const record = await apiHelper.validator.validate(ctx.request.body, AuthLoginPhoneDto);
            const { data, message } = await Service.loginWithEmail(record);
            ApiSuccessHandler.sendJson(ctx, message, data);
            await next();
        },
    ]
);

router.post(
    "/logout",
    ...[
        authUser(),
        async function logout(ctx: Koa.Context, next: Koa.Next) {
            const user = ctx.user;
            const { message } = await Service.logout(user);
            ApiSuccessHandler.sendJson(ctx, message, {});
            await next();
        },
    ]
);

router.post(
    "/access-token",
    ...[
        async function logout(ctx: Koa.Context, next: Koa.Next) {
            const record = await apiHelper.validator.validate(ctx.request.body, RefreshTokenDto);
            const { data, message } = await Service.getAccessToken(record);
            ApiSuccessHandler.sendJson(ctx, message, data);
            await next();
        },
    ]
);


router.get(
    "/user",
    ...[
        authUser(),
        async function user(ctx: Koa.Context, next: Koa.Next) {
            const user = ctx.user;
            const { data, message } = await Service.getUserInfo(user.id);
            ApiSuccessHandler.sendJson(ctx, message, data);
            await next();
        },
    ]
);

router.get(
    "/verify/:userId/:token",
    ...[
        async function verifyEmail(ctx: Koa.Context, next: Koa.Next) {
            const userId = ctx.params.userId
            const token = ctx.params.token
            const { data, message } = await Service.verifyEmail(token, userId);
            if (data.res.active) {
                return ctx.response.redirect(`/login`)
            }
            ApiSuccessHandler.sendJson(ctx, message, data);
            await next();
        },
    ]
);

Controller.use(router.routes()).use(router.allowedMethods());

export default Controller;
