export namespace AuthType {
    type AccessTokenPayload = {
        user: User;
    };
    type RefreshTokenPayload = {
        user: User;
        id?: string;
    };
    type LoginResponse<T> = {
        data: {
            accessToken: string;
            refreshToken: string;
            user: T;
        };
        message: string;
    };
    type NewAccessTokenResponse<T> = {
        data: {
            accessToken: string;
            user: T;
        };
        message: string;
    };
    type LogoutResponse<T> = {
        message: string;
    };
    type RegisterResponse<T> = {
        data: {
            accessToken: string;
            refreshToken: string;
            user: T;
        };
        message: string;
    };
    type User = {
        id: string;
        phone: string;
        email: string;
        token?: string;
    };
    type GetUserResponse<T> = {
        data: {
            user: T;
        };
        message: string;
    };
    type LoginPayloadPhone = {
        phone: string,
        password: string,
    };
    type LoginPayloadEmail = {
        email: string,
        password: string,
        email: string
    }
}

