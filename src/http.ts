import Koa from "koa";
import mount from "koa-mount";

import {
  httpLoggerMiddleware,
  middlewareBodyParser,
  middlewareHandle404,
  middlewareHandleError,
  middlewareHealthCheck,
  middlewareSetResponseHeader,
} from "./libs/middlewares/general";
import UserController from "./modules/user/user.controller";
import AuthController from "./modules/auth/auth.controller";

export const httpServer = new Koa();
httpServer.use(middlewareSetResponseHeader());
httpServer.use(httpLoggerMiddleware());
httpServer.use(middlewareHandleError());
httpServer.use(middlewareBodyParser());

httpServer.use(mount("/api/health", middlewareHealthCheck()));
httpServer.use(mount("/api/v1/auth", AuthController));
httpServer.use(mount("/api/v1/history", UserController));

httpServer.use(middlewareHandle404());