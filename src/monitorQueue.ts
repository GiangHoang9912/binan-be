import { createBullBoard } from "@bull-board/api";
import { KoaAdapter } from "@bull-board/koa";
import { BullAdapter } from "@bull-board/api/bullAdapter";
import Router from "koa-router";
import Koa from "koa";

const app = new Koa();
const router = new Router();
const serverAdapter = new KoaAdapter();
export const bullBoard = createBullBoard({
  queues: [
    // new BullAdapter(domainCrawlerModules.jobs.run__saveUrls.queue, { readOnlyMode: false }),
  ],
  serverAdapter,
});

export async function initAdapter() {
  serverAdapter.setBasePath("/queues");
  app.use(serverAdapter.registerPlugin());
  app.use(router.routes()).use(router.allowedMethods());
  app.listen(3000);
}
