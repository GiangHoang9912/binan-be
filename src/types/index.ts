export type AsyncFunction = (...args: any[]) => Promise<any>;

export interface IRequestFile {
    fileName: string;
    filePath: string;
    fileSize: string;
    fileOriginalName: string;
    fileEncoding: string;
    fileMime: string;
    fileExtension: string;
}

export const RestFindDefault: T__RestFind = {
    search: "",
    page: 1,
    limit: 20,
    filter: {},
    sort: {},
    user: null,
};

export type AppError <T>= {
    message:string,
    status: number,
    code: string,
    data: T|any,
    error: any,
}

export type T__RestFind = {
    search?: string;
    userId?: string;
    page: number;
    limit: number;
    filter: any;
    sort?: any;
    getAll?: boolean;
    user?: any;
    mode?: any;
};

export type T__RestFindResponse<RecordType> = {
    data: {
        total: number;
        page: number;
        pageSize: number;
        limit: number;
        items: RecordType[];
        queryDb?: any;
    };
    message: string;
};
export type T__Response = {
    data: any;
    message: string;
};
export type HistorySearchText<RecordType> = {
    data: RecordType[];
    message: string;
};

export type DeleteHistorySearchText = {
    message: string;
};

