import { createClient } from "redis";
import { getConfig } from "./getConfig";

export const redisCacheClient = createClient({ url: getConfig("REDIS_URI") });

export const connectRedis = () => {
  redisCacheClient.connect();
};
const label = "Redis cache connect";

console.time(label);
redisCacheClient.on("connect", () => {
  console.timeEnd(label);
});

export const cacheHelper = {
  setKeyValueExpire(
    key: string,
    data: any,
    expireInSeconds = 7 * 24 * 60 * 60
  ): any {
    redisCacheClient.setEx(`c:${key}`, expireInSeconds, data);
  },
  getKey(key: string): any {
    return redisCacheClient.get(`c:${key}`);
  },

  delKey(key: string): any {
    redisCacheClient.del(`c:${key}`);
  },

  async getKeyIfNotDoSet(
    key: string,
    handlerReturnData: () => Promise<any>,
    expireInSeconds = 7 * 24 * 60 * 60
  ): Promise<any> {
    let data = await cacheHelper.getKey(`c:${key}`);
    if (!data) {
      data = await handlerReturnData();
      await cacheHelper.setKeyValueExpire(`c:${key}`, data, expireInSeconds);
    }
    return data;
  },
};
