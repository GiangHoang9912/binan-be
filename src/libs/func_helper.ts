import { AsyncFunction } from "../types";

interface funcHelperExecuteOptions {
  label: string;
}

export const funcHelper = {
  async execute(
    func: AsyncFunction,
    options: funcHelperExecuteOptions = { label: "funcHelper.execute" }
  ): Promise<any> {
    const loggingLabel = options.label || "funcHelper.execute";
    console.time(loggingLabel);
    const out = await func();
    console.timeEnd(loggingLabel);
    return out;
  },
};
