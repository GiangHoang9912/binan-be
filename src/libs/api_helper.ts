import {MyError} from "./errors";
import { validateOrReject } from "class-validator";

const is = require("is_js");
import * as R from "ramda";

 const filter = {
    parse(filterProperty: any) {
        if (is.array(filterProperty)) {
            return filterProperty.map((item: any) => filter.parse(item));
        } else if (is.object(filterProperty)) {
            const out: any = {};
            Object.entries(filterProperty).forEach(([key, value]) => {
                if (key.startsWith("$")) {
                    if (key === "$and") {
                        out["AND"] = filter.parse(value);
                    } else if (key === "$or") {
                        out["OR"] = filter.parse(value);
                    } else {
                        out[key.slice(1)] = filter.parse(value);
                    }
                } else {
                    out[key] = filter.parse(value);
                }
            });
            return out;
        } else {
            if (is.number(Number(filterProperty))) {
                return Number(filterProperty);
            }
            if (is.string(filterProperty)) {
                if (filterProperty === "true") {
                    return true;
                }
                if (filterProperty === "null") {
                    return null;
                }
                if (filterProperty === "false") {
                    return false;
                }
                return filterProperty;
            }
            return filterProperty;
        }
    },

     parseFilterPostgre(filterQuery: any) {
         if (is.object(filterQuery)) {
             const out: any = {};
             Object.entries(filterQuery).forEach(([key, value]) => {
                 out[key] = filter.parse(value);
             });

             return out;
         }
     },

    parseSort(sort: any) {
        sort = sort || {};
        const arr = Object.entries(sort);
        let out = {};
        for (const [keyHasDot, value] of arr) {
            const keys = keyHasDot.split(".");
            let acc = {};
            if (value === 1 || value === "1") {
                acc = R.assocPath(keys, "asc", acc);
            }
            if (value === -1 || value === "-1") {
                acc = R.assocPath(keys, "desc", acc);
            }
            out = {...out, ...acc};
        }

        return out;
    },

    combineFilter(...filters: any[]) {
        let out = {
            // @ts-ignore
            AND: [],
            // @ts-ignore
            OR: [],
        };
        for (const filter of filters) {
            const {AND, OR, ..._filter} = filter;
            if (AND && AND.length) {
                out.AND = [...out.AND, ...AND];
            }
            if (OR && OR.length) {
                out.OR = [...out.OR, ...OR];
            }
            out = {...out, ..._filter};
        }
        if (out.AND.length === 0) {
            delete out.AND;
        }
        if (out.OR.length === 0) {
            delete out.OR;
        }
        return out;
    },

    searchFields(textSearch: any, fields: any){
         const output:any = [];
        console.log(fields);
         fields.map((field: string) => {
             const objectField: any = {};
             objectField[`${field}`] = { "contains": textSearch.replace("*", "").trim() };
             output.push(objectField);
         });
         const out = {OR: output};
         console.log(out);
         return out;
     },
     searchFieldMongoDB(textSearch: any, fields: any){
         const search = textSearch.replace("+", "").trim();
         const searchQuery = fields.map((item: string) => ({
             [item]: {
                 $regex: new RegExp(search, "i"),
             },

         }));
         return {$or: searchQuery};
     },
};

const validator = {
    async validate(data: any, dto: any) {
        const obj = new dto();
        for (const key in data) {
            obj[key] = data[key];
        }
        try {
            await validateOrReject(obj);
            return obj;
        } catch (e) {
            throw MyError.NotValidate(e.map((item: any) => Object.values(item.constraints).join("\n")).join("\n"), {
                error: e,
            });
        }
    },
};
export const apiHelper = {
    filter,
    validator,
};
