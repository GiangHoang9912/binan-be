import { exec } from "child_process";
import { format as dateFormat } from "date-fns";
import { MyError } from "./errors";
import rp from "request-promise";
import requestPromise from "request-promise";
import mongoose from "mongoose";
import crypto from "crypto";
import URL from "url";
import fs from "fs";
import { getConfig } from "./getConfig";
import parseDur from "parse-duration";
import { v1 as getUuid } from "uuid";
import cheerio from "cheerio";
import http from "http";
import https from "https";
import path from "path";
import * as nodemailer from "nodemailer"
const handlebars = require('handlebars');

const urlExists = require("url-exists");

const readline = require("readline");

// @ts-ignore
export const helper = {
    async runCmd(cmd: string, options: any = {}) {
        const now = new Date();
        return new Promise((resolve: any, reject: any) => {
            exec(cmd, options, async (err, stdout, stderr) => {
                if (err) {
                    if (options?.timeout) {
                        const duration = new Date().getTime() - now.getTime();
                        if (duration > options.timeout) {
                            return reject(MyError.ServerError(`Timeout after ${duration}ms`));
                        }
                    }
                    return reject(MyError.ServerError(`cant start '${cmd}' ${err}`, err));
                }
                // the *entire* stdout and stderr (buffered)
                // console.log(`stdout: ${stdout}`);
                // console.log(`stderr: ${stderr}`);
                return resolve(stdout);
            });
        });
    },
    getNewUuid() {
        return getUuid();
    },
    escapeRegExp(string: string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&").replace(/\//g, "\\/");
    },
    async isAccessAblePath(inputPath: string, showError = false) {
        let accessAble = true;
        try {
            await fs.promises.access(inputPath);
        } catch (err) {
            if (showError) {
                console.log("err", err);
            }
            accessAble = false;
        }
        return accessAble;
    },
    getFolderDate(mode = "date") {
        if (mode === "date") {
            return `${dateFormat(new Date(), "yyyy_LL_dd")}`;
        }
        if (mode === "time") {
            return `${dateFormat(new Date(), "HH_mm")}`;
        }
        if (mode === "timeDetail") {
            return `${dateFormat(new Date(), "HH_mm_ss")}`;
        }
        if (mode === "full") {
            return `${dateFormat(new Date(), "yyyy_LL_dd_HH_mm_ss")}`;
        }
        return `${dateFormat(new Date(), "yyyy_LL_dd")}`;
    },
    genMd5(text: string): string {
        return crypto.createHash("md5").update(text).digest("hex");
    },
    getMongoId(id: string) {
        return new mongoose.Types.ObjectId(id);
    },
    handlerError(...args: any[]) {
        console.log(args);
        return args;
    },
    getUrlWithProtocol(url: string) {
        let realUrl;
        if (url.startsWith("http")) {
            realUrl = new URL.URL(url);
        } else {
            realUrl = new URL.URL("http://" + url);
        }

        return realUrl;
    },
    async saveFileWithPath(url: string, realPath: string) {
        const realUrl = new URL.URL(url);
        const file = fs.createWriteStream(realPath);
        let client: any = http;
        if (realUrl.toString().indexOf("https") === 0) {
            client = https;
        }

        client
            .get(realUrl.toString(), async (response: any) => {
                await response.pipe(file);
                file.on("finish", function () {
                    file.close();
                });
            })
            .on("error", function (err: any) {
                fs.unlink(realPath, () => console.log(err.message));
            });

        return true;
    },
    async urlExists(urlCheck: string) {
        const realUrl = helper.getUrlWithProtocol(urlCheck);
        console.log("realUrl", realUrl.host);
        return await new Promise((resolve: any) => {
            urlExists(realUrl.toString(), function (err: any, exists: any) {
                console.log("exists", exists);
                if (exists) resolve(true);
                resolve(false);
            });
        });
    },
    async makeRequest(options: requestPromise.Options) {
        return rp({
            ...options,
        });
    },
    async loadPage(options: requestPromise.Options) {
        try {
            const html = await rp({
                ...options,
                followAllRedirects: true,
                rejectUnauthorized: false,
            });
            const $ = cheerio.load(html);
            const redirectContent = $("[http-equiv=\"refresh\"]").first().attr("content");
            if (redirectContent) {
                const redirectInfo = this.getUrlFromMetaRedirectContent(redirectContent);
                if (redirectInfo) {
                    if (redirectInfo.url.indexOf("http") === 0) {
                        const _options = {
                            ...options,
                            url: redirectInfo.url,
                        };
                        return await this.loadPage(_options);
                    } else {
                        //@ts-ignore
                        const _baseUrl = options?.uri || options?.url;
                        const rootUrl = new URL.URL(_baseUrl);
                        let redirectPath;
                        if (redirectInfo.url.indexOf("/") === 0) {
                            redirectPath = redirectInfo.url;

                        } else {
                            redirectPath = path.join(rootUrl.pathname, redirectInfo.url);

                        }
                        const redirectUrl = new URL.URL(redirectPath, rootUrl.toString());
                        const _options = { ...options, url: redirectUrl.toString() };
                        return await this.loadPage(_options);
                    }
                }
            }
            return {
                html,
                httpCode: 200,
                $,
                // @ts-ignore
                url: options?.url,
                message: "Thành công",
            };
        } catch (e) {
            return {
                html: "",
                $: cheerio.load("", { decodeEntities: true }),
                httpCode: e.statusCode,
                // @ts-ignore
                url: options?.url,
                message: e.message,
            };
        }
    },
    getUrlFromMetaRedirectContent(_content: string) {
        const PATTERN = /^\s*(\d+)(?:\s*;(?:\s*url\s*=)?\s*(?:["']\s*(.*?)\s*['"]|(.*?)))?\s*$/i;

        const content = PATTERN.exec(_content);

        let timeout, url;

        if (content !== null) {
            timeout = parseInt(content[1], 10);

            url = content[2] || content[3] || null; // first matching group
        } else {
            timeout = null;
            url = null;
        }

        return { timeout, url };
    },
    changeToSlug(str: string, replacer = "-") {
        // Chuyển hết sang chữ thường
        str = str.toLowerCase();

        // xóa dấu
        str = str.replace(/([àáạảãâầấậẩẫăằắặẳẵ])/g, "a");
        str = str.replace(/([èéẹẻẽêềếệểễ])/g, "e");
        str = str.replace(/([ìíịỉĩ])/g, "i");
        str = str.replace(/([òóọỏõôồốộổỗơờớợởỡ])/g, "o");
        str = str.replace(/([ùúụủũưừứựửữ])/g, "u");
        str = str.replace(/([ỳýỵỷỹ])/g, "y");
        str = str.replace(/(đ)/g, "d");

        // Xóa ký tự đặc biệt
        str = str.replace(/([^0-9a-z-\s])/g, "");

        // Xóa khoảng trắng thay bằng ký tự -
        str = str.replace(/(\s+)/g, replacer);

        // xóa phần dự - ở đầu
        str = str.replace(/^-+/g, "");

        // xóa phần dư - ở cuối
        str = str.replace(/-+$/g, "");

        // return
        return str;
    },
    async wait(ms: number): Promise<void> {
        return new Promise((resolve: any) => setTimeout(resolve, ms));
    },
    getSha1(text: string): string {
        const key = getConfig("app.secret");
        return crypto.createHmac("sha1", key).update(text).digest("hex");
    },
    // @param {string} time_text - Duration in englist like 10 minutes,... check human-intervals
    // @returns {number} - miliseconds
    getDurationFromText(duration_string: string): number {
        return parseDur(duration_string, "ms");
    },
    detectObjectFromString(text: string): any {
        const array = text.split(/\s{2,}/g);
        return {
            firstValue: array[0] ? array[0] : null,
            secondValue: array[1] ? array[1] : null,
        };
    },
    onlyUnique(array: any[], files: string): any[] {
        return [...new Map(array.map((item) => [item[`${files}`], item])).values()];
    },
    async getFileExtension(filePath: string, exiftool: boolean): Promise<string> {
        if (exiftool) {
            const infoExiftool: any = await helper.runCmd(`exiftool ${filePath}`);
            const info: any = infoExiftool.split("\n").reduce((acc: any, cur: any) => {
                const indexOfTwoDot = cur.indexOf(":");
                const key = cur.slice(0, indexOfTwoDot).trim();
                const value = cur.slice(indexOfTwoDot + 1).trim();
                acc[key] = value;
                return acc;
            }, {});
            return String(info["File Type Extension"]);
        } else {
            if (filePath) {
                return filePath.split(".").pop();
            } else {
                return "";
            }
        }
    },
    async processLineByLine(filePath: string): Promise<string[]> {
        const fileStream = fs.createReadStream(filePath);

        const lines: string[] = [];

        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity,
        });

        for await (const line of rl) {
            lines.push(line);
        }

        return lines;
    },

    parseQueryString(string: string) {
        if (string === "" || string == null) return {};
        if (string.charAt(0) === "?") string = string.slice(1);

        const entries = string.split("&"),
            counters = {},
            data = {};
        for (let i = 0; i < entries.length; i++) {
            const entry = entries[i].split("=");
            // eslint-disable-next-line no-var
            var key = decodeURIComponent(entry[0]);
            let value = entry.length === 2 ? decodeURIComponent(entry[1]) : "";

            if (value === "true") {
                // @ts-ignore
                value = true;
            }
            // @ts-ignore
            else if (value === "false") value = false;

            const levels = key.split(/\]\[?|\[/);
            let cursor = data;
            if (key.indexOf("[") > -1) levels.pop();
            for (let j = 0; j < levels.length; j++) {
                let level = levels[j],
                    // @ts-ignore
                    // eslint-disable-next-line prefer-const
                    nextLevel = levels[j + 1];
                const isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
                if (level === "") {
                    // @ts-ignore
                    // eslint-disable-next-line no-var
                    var key = levels.slice(0, j).join();
                    // @ts-ignore
                    if (counters[key] == null) {
                        // @ts-ignore
                        counters[key] = Array.isArray(cursor) ? cursor.length : 0;
                    }

                    // @ts-ignore
                    level = counters[key]++;
                }
                // Disallow direct prototype pollution
                else if (level === "__proto__") break;

                // @ts-ignore
                if (j === levels.length - 1) cursor[level] = value;
                else {
                    // Read own properties exclusively to disallow indirect
                    // prototype pollution
                    let desc = Object.getOwnPropertyDescriptor(cursor, level);
                    if (desc != null) desc = desc.value;
                    // @ts-ignore
                    if (desc == null) cursor[level] = desc = isNumber ? [] : {};
                    cursor = desc;
                }
            }
        }
        return data;
    },
    isMongoId(id: string): boolean {
        return mongoose.Types.ObjectId.isValid(id);
    },
    sendmail(params: { email: string, content?: string, subject?: string, text?: string, context?: any }) {
        const { email, content, subject, text, context } = params
        console.log(getConfig("EMAIL.email"))
        console.log(getConfig("EMAIL.password"))
        let transporter = nodemailer.createTransport({ // config mail server
            service: getConfig("EMAIL.service"),
            auth: {
                user: getConfig("EMAIL.email"),
                pass: getConfig("EMAIL.password"),
            }
        });

        const template = handlebars.compile(content);

        const htmlToSend = template(context);

        const mainOptions = { // thiết lập đối tượng, nội dung gửi mail
            from: `${getConfig("app.name")} <noreply.${getConfig("EMAIL.email")}>`,
            replyTo: `noreply.${getConfig("EMAIL.email")}`,
            to: email,
            subject: subject,
            text: text,
            html: htmlToSend,
        }
        transporter.sendMail(mainOptions, function (err, info) {
            if (err) {
                console.log(err);
            } else {
                console.log('Message sent: ' + info.response);
            }
        });
    }
};
