import mongoose from "mongoose";
import {getConfig} from "../../getConfig";

const options = {
    useNewUrlParser: true,
    useFindAndModify: false,
    connectTimeoutMS: 5000,
    useUnifiedTopology: true,
    useCreateIndex: true

};
const url = getConfig("DB_URI");
export const dbConnect = mongoose.createConnection(url, options);
dbConnect.on("error", err => console.error("DB cant connect", url, err));
dbConnect.on("connected", () => console.error("DB connected", url));

