import mongoose from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";
import {dbConnect} from "./connection";

// Tham khảo
// https://stackoverflow.com/questions/23512683/how-to-format-data-in-model-before-saving-in-mongoose-expressjs/23519211


const _SampleSchema = new mongoose.Schema(
    {
        text: {
            type: String,
            validate: {
                validator: (value:any) => value,
                message: (prop:any) => "value must be truthy"
            },
            default: "truthy text"
        },
        createdBy: {
            name: {type: String},
            id: {type: String}
        },
        textShouldBeLower: {
            type: String, set(val:any, schemaType:any) {
                return val.toLowerCase();
            }
        },
        _status: {type: Number, default: 1} //1 : active, 0 :deleted
    },
    {
        timestamps: {
            createdAt: "createdAt",
            updatedAt: "updatedAt"
        },
        toJSON: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
            }
        }
    }
);

_SampleSchema.plugin(mongoosePaginate);
export const _sampleModel = dbConnect.model("_Samples", _SampleSchema, "_Samples");
