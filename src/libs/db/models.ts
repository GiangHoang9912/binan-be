import { PrismaClient } from "@prisma/client";

export const prisma = new PrismaClient();
export const Models = {
  user: prisma.user,
  token: prisma.token
};
