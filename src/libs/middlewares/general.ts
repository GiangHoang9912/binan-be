import session from "koa-session";
import bodyParser from "koa-bodyparser";
import multer from "@koa/multer";
import cors from "@koa/cors";
import Koa, { BaseContext, Next } from "koa";
import { ApiSuccessHandler } from "../koa_helper";
import pino from "koa-pino-logger";
import KoaQueryString from "koa-qs";

import { getConfig } from "../getConfig";


export const middlewareQueryString = (app: Koa) => {
    KoaQueryString(app, "extended");
};
export const middlewareSession = (app: any, options = {}) => {
    const CONFIG = {
        key: getConfig("COOKIES_KEY") /** (string) cookie key (default is koa.sess) */,
        /** (number || 'session') maxAge in ms (default is 1 days) */
        /** 'session' will result in a cookie that expires when session/browser is closed */
        /** Warning: If a session cookie is stolen, this cookie will never expire */
        maxAge: 86400000,
        autoCommit: true,
        /** (boolean) automatically commit headers (default true) */
        overwrite: true,
        /** (boolean) can overwrite or not (default true) */
        httpOnly: true,
        /** (boolean) httpOnly or not (default true) */
        signed: false,
        /** (boolean) signed or not (default true) */
        rolling: false,
        /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
        renew: false,
        /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
        secure: false,
        /** (boolean) secure cookie*/
        // sameSite: null /** (string) session cookie sameSite options (default null, don't set it) */
    };
    return session(CONFIG, app);
};

export const middlewareBodyParser = (options = {}) => {
    return bodyParser(options);
};

export const middlewareEndServices = (): Koa.Middleware => {
    return async (ctx: BaseContext, next: Next) => {
        return true;
    };
};

export const httpLoggerMiddleware = (): Koa.Middleware => {
    return pino();
};

export const middlewareUpload = (params: any = {}) => {
    const dest = params.dest ?? getConfig("DIR_TMP");
    const fieldSize: any = params.fieldSize ?? getConfig("UPLOAD_MAX_SIZE");
    const fields = params.fields ?? [{ name: "files", maxCount: 1 }];

    return multer({
        dest,
        limits: {
            fieldSize,
        },
    }).fields(fields); // note you can pass `multer` options here
};

export const middlewareCors = () => {
    return cors();
};

export function middlewareHealthCheck(message: string = ""): Koa.Middleware {
    return function (ctx: BaseContext, next: Next) {
        ApiSuccessHandler.sendJson(ctx, "service is working 2", {
            date: new Date(),
            message,
        });
    };
}

export const middlewareHandle404 = (options = {}) => {
    return async (ctx: Koa.Context) => {
        if (!ctx.status) {
            ctx.status = 404;
            ctx.body = { message: "404 Not found" };
        }
    };
};

export const middlewareHandleError = (options = {}): Koa.Middleware => {
    return async (ctx: Koa.Context, next) => {
        try {
            await next();
        } catch (err) {
            ctx.status = err.status || 500;
            ctx.body = {
                code: err.code || err.status,
                status: err.status,
                message: err.message,
                data: err.data,
            };
            if (process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev") {
                console.error(err);
                // ctx.app.emit('error', err, ctx);
            } else if (process.env.NODE_ENV === "production") {
                console.error(err);
            } else {
                console.error(err);
            }
        }
    };
};

export const middlewareSetResponseHeader = (option = {}): Koa.Middleware => {
    return async (ctx: Koa.Context, next: Koa.Next) => {
        const start = Date.now();
        await next();
        const ms = Date.now() - start;
        ctx.set("X-Response-Time", `${ms}ms`);
    };
};
