import {helper} from "./libs/helper";
import seedingAuth from "./modules/auth/auth.seed";

export async function init() {
    async function seeding() {
        try {
            // await seedingAuth();
        } catch (e) {
           throw helper.handlerError("seeding error", e);
        }
    }

    seeding();
}

