const puppeteer = require("puppeteer");
const cheerio = require("cheerio");
const SHOPPE_URL = "https://shopee.vn/"
(async () => {
    const browser = await puppeteer.launch({});
    const page = await browser.newPage();
    await page.goto("https://shopee.vn/search?keyword=gi%C3%A1%20treo%20m%C3%A0n%20h%C3%ACnh", {
        waitUntil: "networkidle2"
    });
    const content = await page.content();
    const $ = cheerio.load(content);
    const listProduct = $("#main > div > div > div > div > div> div.shopee-search-item-result > div.row.shopee-search-item-result__items > div > a");
    const products = listProduct.map((i, dom) => $(dom).attr("href"));
    console.log(products.toArray());

    await page.screenshot({path: "example.png"});

    // await browser.close();
})();
