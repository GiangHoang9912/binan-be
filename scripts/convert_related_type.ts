import {helper} from "../src/libs/helper";

const typeRelatedStr = `
  Văn bản được HD, QĐ chi tiết
  Văn bản hiện thời
  Văn bản HD, QĐ chi tiết
  Văn bản hết hiệu lực
  Văn bản căn cứ
  Văn bản quy định hết hiệu lực
  Văn bản bị hết hiệu lực 1 phần
  Văn bản dẫn chiếu
  Văn bản quy định hết hiệu lực 1 phần
  Văn bản bị đình chỉ
  Văn bản liên quan khác
  Văn bản đình chỉ
  Văn bản bị đình chỉ 1 phần
  Văn bản đình chỉ 1 phần
  Văn bản được bổ sung 
  Văn bản bổ sung
  Văn bản được sửa đổi    
  Văn bản sửa đổi    
`;

const typeDocs = `
Hiến pháp
Bộ luật
Luật
Pháp lệnh
Lệnh
Nghị quyết
Nghị quyết liên tịch
Nghị định
Quyết định
Thông tư
Thông tư liên tịch`;

function genEnum(input: string) {
    const out = input.split("\n").map(item => item.trim()).map(item => helper.changeToSlug(item, "_")).map(item => item.toUpperCase()).join("\n");
    console.log(out);
    return out;
}
genEnum(typeDocs);
