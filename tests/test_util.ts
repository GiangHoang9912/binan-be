import { jest } from "@jest/globals";
import * as request from "supertest";

//chạy ứng dụng
jest.useFakeTimers();

import { httpServer } from "../src/http";

const httpServerInstance = httpServer.callback();

function doRequest() {
  return request(httpServerInstance);
}

export const testUtil = {
  httpServer,
  doRequest,
};
