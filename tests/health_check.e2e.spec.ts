import { testUtil } from "./test_util";

describe("Health check", () => {

  test("GET /api/health", async () => {
    const response = await testUtil.doRequest().get("/api/health");
    expect(response.status).toBe(200);
  });
});
